# Shop demo qa 

### Table of Content
- [Introduction](#Introduction)
- [Prerequisites](#Prerequisites)
- [Running tests locally](#Running-tests-locally)
- [Technology stack](#Technology-stack)

## Introduction

This project contains UI functional and visual automated tests for Shop demo qa

### Prerequisites
- [Java 19](https://download.java.net/java/early_access/loom/docs/api/)
- [Maven](https://maven.apache.org/)
- [Chrome browser has to be installed](https://www.google.com/chrome/?brand=CHBD&gclid=Cj0KCQjwgJv4BRCrARIsAB17JI7-EIZCQfo4Je5O0LNbMME2sYLjDzlcpM6zeRW81u3ehLyeK8Ebz7MaAnnIEALw_wcB&gclsrc=aw.ds)
- [Setup default IDE formatting tool](https://gitlab.com/rit-automation/web-functional-automation/wiki/Setup-default-IDE-formatting-tool)
- [Extent report](https://www.extentreports.com/docs/versions/5/net/index.html)


# Run all tests
mvn clean test

# Run single test
mvn test -Dtest="<Test_Class_Name>"
# example
mvn test -Dtest="AddItemToWishListTest"


### Technology stack
- [Java 19](https://download.java.net/java/early_access/loom/docs/api/)
- [Maven](https://maven.apache.org/)
- [Selenium WebDriver](https://www.selenium.dev/)
- [JUnit](https://junit.org/junit4/javadoc/latest/)
- [Extent report](https://www.extentreports.com/docs/versions/5/net/index.html)

### Report generation
# Right click on "report.html" file and "Open IN -> Browser -> Chrome"
