package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.logging.Logger;

/**
 * All elements are not the actual elements And I assumed them and coded.
 */
public class ItemPage extends BaseClass {
    By lblItemTitle = By.xpath("//div/h1[@class='product_title entry-title']");
    By ddColour = By.id("pa_color");
    By ddSize = By.id("pa_size");
    By btnAddToCart = By.xpath("//button[contains(text(),'Add to cart')]");
    By btnCart = By.xpath("//a[@title='View cart']");
    By btnAddToWishList = By.cssSelector("div.summary.entry-summary>div>div.yith-wcwl-add-button");

    public ItemPage(WebDriver driver) {
        super(driver);
    }

    public String getItemTitle() {
        return getElementText(lblItemTitle);
    }

    public void selectColour(String colour) {
        selectByVisibleText(ddColour, colour);
    }

    public void selectSize(String size) {
        selectByVisibleText(ddSize, size);
    }

    public void clickOnAddToCart() {
        waitTillExpectedCondition(
                ExpectedConditions.elementToBeClickable(btnAddToCart), TIMEOUT_TEN_SECONDS);
        click(btnAddToCart);
    }

    public void clickOnCart() {
        waitTillExpectedCondition(
                ExpectedConditions.elementToBeClickable(btnCart), TIMEOUT_TEN_SECONDS);
        click(btnCart);
    }

    public void clickAddProductToWishList() {
        waitTillExpectedCondition(
                ExpectedConditions.visibilityOfElementLocated(btnAddToWishList), TIMEOUT_TWENTY_SECONDS);
        for(int i=0; i<=2;i++){
            try{
                click(btnAddToWishList);
                break;
            }
            catch(StaleElementReferenceException e){
            Logger.getLogger(e.getMessage());
            }
        }

    }
}
