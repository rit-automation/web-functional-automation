package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * All elements are not the actual elements And I assumed them and coded.
 */
public class CartPage extends BaseClass {
    By lblItemTitle = By.xpath("(//tr[@class='cart_item']/td/a)[2]");
    By lblSie = By.xpath("//tr[@class='cart_item']/td/dl/dd/p");
    By btnViewCart = By.xpath("//a[contains(text(),'View cart')]");
    By btnCheckout = By.xpath("//a[contains(text(),'Proceed to checkout')]");

    public CartPage(WebDriver driver) {
        super(driver);
    }

    public String getItemTitle() {
        return getElementText(lblItemTitle);
    }

    public String getItemSize() {
        return getElementText(lblSie);
    }

    public void clickOnViewCart() {
        waitTillExpectedCondition(
                ExpectedConditions.elementToBeClickable(btnViewCart), TIMEOUT_TEN_SECONDS);
        click(btnViewCart);
    }

    public void clickOnProceedToCheckout() {
        waitTillExpectedCondition(
                ExpectedConditions.elementToBeClickable(btnCheckout), TIMEOUT_TEN_SECONDS);
        click(btnCheckout);
    }
}
