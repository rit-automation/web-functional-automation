package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class WishlistPage extends BaseClass{
    By productNames = By.cssSelector("tbody>tr>td.product-name>a");

    public WishlistPage(WebDriver driver) {
        super(driver);
    }

    public List<String> getAddedWishlistProducts() {
        List<WebElement> elementList = getWebElementList(productNames);
        return getTextListFromElementList(elementList);
    }
}
