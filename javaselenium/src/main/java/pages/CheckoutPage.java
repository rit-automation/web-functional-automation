package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * All elements are not the actual elements And I assumed them and coded.
 */
public class CheckoutPage extends BaseClass {
    By btnCheckout = By.xpath("//a[contains(text(),'Proceed to checkout')]");
    By tfFname = By.id("billing_first_name");
    By tfLname = By.id("billing_last_name");
    By tfAddress = By.id("billing_address_1");
    By tfCity = By.id("billing_city");
    By tfPostalCode = By.id("billing_postcode");
    By tfPhone = By.id("billing_phone");
    By tfEmail = By.id("billing_email");
    By chkTerms = By.xpath("//input[@id='terms']");
    By btnPlaceOrder = By.id("place_order");
    By confirmationMessage = By.cssSelector("div>p.woocommerce-thankyou-order-received");
    By blockUi = By.cssSelector("div.blockUI.blockOverlay");

    public CheckoutPage(WebDriver driver) {
        super(driver);
    }

    public void typeFirstName(String firstName) {
        waitTillExpectedCondition(
                ExpectedConditions.visibilityOfAllElementsLocatedBy(tfFname), TIMEOUT_TEN_SECONDS);
        enterText(tfFname, firstName);
    }

    public void typeLastName(String lastName) {
        waitTillExpectedCondition(
                ExpectedConditions.visibilityOfAllElementsLocatedBy(tfLname), TIMEOUT_TEN_SECONDS);
        enterText(tfLname, lastName);
    }

    public void typeAddress(String address) {
        waitTillExpectedCondition(ExpectedConditions
                .visibilityOfAllElementsLocatedBy(tfAddress), TIMEOUT_TEN_SECONDS);
        enterText(tfAddress, address);
    }

    public void typeCity(String city) {
        waitTillExpectedCondition(
                ExpectedConditions.visibilityOfAllElementsLocatedBy(tfCity), TIMEOUT_TEN_SECONDS);
        enterText(tfCity, city);
    }

    public void typePostalCode(String postalCode) {
        waitTillExpectedCondition(ExpectedConditions
                .visibilityOfAllElementsLocatedBy(tfPostalCode), TIMEOUT_TEN_SECONDS);
        enterText(tfPostalCode, postalCode);
    }

    public void typePhone(String phone) {
        waitTillExpectedCondition(
                ExpectedConditions.visibilityOfAllElementsLocatedBy(tfPhone), TIMEOUT_TEN_SECONDS);
        enterText(tfPhone, phone);
    }

    public void typeEmail(String email) {
        waitTillExpectedCondition(
                ExpectedConditions.visibilityOfAllElementsLocatedBy(tfEmail), TIMEOUT_TEN_SECONDS);
        enterText(tfEmail, email);
    }

    public void clickOnTerms() {
        scrollToElement(tfAddress);
        waitTillExpectedCondition(
                ExpectedConditions.invisibilityOfElementLocated(blockUi), TIMEOUT_TWENTY_SECONDS);
        click(chkTerms);
    }

    public void clickOnPlaceOrder() {
        waitTillExpectedCondition(
                ExpectedConditions.elementToBeClickable(btnPlaceOrder), TIMEOUT_TWENTY_SECONDS);
        click(btnPlaceOrder);
    }

    public String getConfirmationMessage() {
        waitTillExpectedCondition(ExpectedConditions.elementToBeClickable(confirmationMessage),
                TIMEOUT_TWENTY_SECONDS);
       return getElementText(confirmationMessage);
    }
}
