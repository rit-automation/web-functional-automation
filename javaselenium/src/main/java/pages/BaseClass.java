package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This class contains some common code used in page layer
 */
public class BaseClass {
    protected static final int TIMEOUT_TEN_SECONDS = 10;
    protected static final int TIMEOUT_TWENTY_SECONDS = 20;
    protected static final int TIMEOUT_FORTY_SECONDS = 40;

    WebDriver driver;

    protected BaseClass(WebDriver driver) {
        this.driver = driver;
    }

    <T> void waitTillExpectedCondition(ExpectedCondition<T> expectedCondition, int timeToWait) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, timeToWait);
        webDriverWait.until(expectedCondition);
    }

    protected void selectByVisibleText(By element, String visibleText) {
        Select select = new Select(driver.findElement(element));
        select.selectByVisibleText(visibleText);
    }

    protected WebElement expandRootElement(WebElement element) {
        WebElement rootElement = (WebElement) ((JavascriptExecutor) driver)
                .executeScript("return arguments[0].shadowRoot", element);
        return rootElement;
    }

    protected void click(By by) {
        driver.findElement(by).click();
    }

    protected void enterText(By by, String text) {
        driver.findElement(by).sendKeys(text);
    }

    protected void scrollToElement(By by) {
        WebElement webElement = driver.findElement(by);
        ((JavascriptExecutor) driver)
                .executeScript("arguments[0].scrollIntoView(true);", webElement);
    }

    protected String getElementText(By by) {
        return driver.findElement(by).getText();
    }

    protected void pressKeyForElement(By by, Keys key) {
        driver.findElement(by).sendKeys(key);
    }

    protected List<WebElement> getWebElementList(By by) {
        return driver.findElements(by);
    }

    protected List<String> getTextListFromElementList(List<WebElement> elementList) {
        return elementList.stream().map(webElement -> webElement.getText())
                .collect(Collectors.toList());
    }

    public void openPage(String url) {
        driver.navigate().to(url);
    }

    public void refreshPage() {
        driver.navigate().refresh();
    }
}
