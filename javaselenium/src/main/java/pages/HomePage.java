package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * All elements are not the actual elements And I assumed them and coded.
 */
public class HomePage extends BaseClass {
    By lblSearch = By.xpath("//a[@class='noo-search']/i");
    By txtSearch = By.xpath("//input[@type='search']");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void typeSearchKeyword(String keyword) {
        waitTillExpectedCondition(ExpectedConditions
                .visibilityOfAllElementsLocatedBy(txtSearch), TIMEOUT_TEN_SECONDS);
        enterText(txtSearch, keyword);
    }

    public void clickOnSearch() {
        waitTillExpectedCondition(
                ExpectedConditions.elementToBeClickable(lblSearch), TIMEOUT_TEN_SECONDS);
        click(lblSearch);
    }

    public void hitEnterKey() {
        pressKeyForElement(txtSearch, Keys.ENTER);
    }
}
