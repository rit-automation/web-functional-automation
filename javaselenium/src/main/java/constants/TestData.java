package constants;

public interface TestData {
    public static final String SEARCH_TXT = "PINK DROP SHOULDER OVERSIZED T SHIRT";
    public static final String COLOUR = "Pink";
    public static final String SIZE = "36";
    public static final String FIRST_NAME = "Adam";
    public static final String LAST_NAME = "Smith";
    public static final String ADDRESS = "130, Park Avenue";
    public static final String CITY = "Colombo";
    public static final String POSTAL_CODE = "111800";
    public static final String PHONE = "1234567890";
    public static final String EMAIL = "adam.smith@abc.com";
    public static final String CONFIRMATION_MESSAGE = "Thank you. Your order has been received.";
}
