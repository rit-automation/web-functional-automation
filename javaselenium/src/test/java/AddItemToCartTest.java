import constants.TestData;
import org.junit.ComparisonFailure;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;


public class AddItemToCartTest extends TestBase implements TestData {

    @Test
    public void addItemIntoShoppingCart() throws IOException {
        try {
            test = extentReports.createTest("Shopping Cart Demo",
                    "Add an item into shopping cart");
            homePage.clickOnSearch();
            homePage.typeSearchKeyword(SEARCH_TXT);
            homePage.hitEnterKey();

            String itemTitle = itemPage.getItemTitle();
            assertEquals(itemTitle, SEARCH_TXT);
            test.pass(itemTitle);

            itemPage.selectColour(COLOUR);
            itemPage.selectSize(SIZE);
            itemPage.clickOnAddToCart();
            itemPage.clickOnCart();

            String title = SEARCH_TXT + " - " + COLOUR.toUpperCase();
            String size = cartPage.getItemSize();
            assertEquals(cartPage.getItemTitle(), title);
            test.pass(title);
            assertEquals(size, SIZE);
            test.pass(size);
        } catch (ComparisonFailure e) {
            test.fail(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            test.fail(e.getMessage());
            e.printStackTrace();
        }
    }
}
