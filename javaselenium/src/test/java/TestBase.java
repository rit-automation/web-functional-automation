import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import constants.Constants;
import factory.PageFactory;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

public class TestBase {
    protected static WebDriver driver;

    protected static HomePage homePage;
    protected static ItemPage itemPage;
    protected static CartPage cartPage;
    protected static WishlistPage wishlistPage;
    protected static CheckoutPage checkoutPage;
    protected static ExtentHtmlReporter htmlReporter;
    protected static ExtentReports extentReports;
    protected static ExtentTest test;


    @Before
    public void driverSetup() {
        ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();
        driver = new ChromeDriver();
        driver.navigate().to(Constants.URL);
        driver.manage().window().maximize();
        if(extentReports==null) {
            htmlReporter = new ExtentHtmlReporter("report.html");
            extentReports = new ExtentReports();
            extentReports.attachReporter(htmlReporter);
        }
    }

    @Before
    public void objectSetup() {
        PageFactory pageFactory = new PageFactory();
        homePage = (HomePage) pageFactory.getPageObject(driver, "Home Page");
        itemPage = (ItemPage) pageFactory.getPageObject(driver, "Item Page");
        cartPage = (CartPage) pageFactory.getPageObject(driver, "Cart Page");
        wishlistPage = (WishlistPage) pageFactory.getPageObject(driver, "Wishlist Page");
        checkoutPage = (CheckoutPage) pageFactory.getPageObject(driver, "Checkout Page");
    }

    @After
    public void quitBrowser() {
        extentReports.flush();
        driver.quit();
    }
}
