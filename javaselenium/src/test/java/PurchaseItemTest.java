import constants.TestData;
import org.junit.ComparisonFailure;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class PurchaseItemTest extends TestBase implements TestData {

    @Test
    public void purchaseItem() {
        try {
            test = extentReports.createTest("Test Purchase Item",
                    "Purchase an Item");
            homePage.clickOnSearch();
            homePage.typeSearchKeyword(SEARCH_TXT);
            homePage.hitEnterKey();

            String itemTitle = itemPage.getItemTitle();
            assertEquals(itemTitle, SEARCH_TXT);
            test.pass(itemTitle);

            itemPage.selectColour(COLOUR);
            itemPage.selectSize(SIZE);
            itemPage.clickOnAddToCart();
            itemPage.clickOnCart();

            String title = cartPage.getItemTitle();

            assertEquals(title, SEARCH_TXT + " - " + COLOUR.toUpperCase());
            test.pass(title);
            String size = cartPage.getItemSize();
            assertEquals(size, SIZE);
            test.pass(size);
            cartPage.clickOnProceedToCheckout();

            checkoutPage.typeFirstName(FIRST_NAME);
            checkoutPage.typeLastName(LAST_NAME);
            checkoutPage.typeAddress(ADDRESS);
            checkoutPage.typeCity(CITY);
            checkoutPage.typePostalCode(POSTAL_CODE);
            checkoutPage.typePhone(PHONE);
            checkoutPage.typeEmail(EMAIL);
            checkoutPage.clickOnTerms();
            checkoutPage.clickOnPlaceOrder();

            String confirmationMessage = checkoutPage.getConfirmationMessage();
            assertEquals(confirmationMessage, CONFIRMATION_MESSAGE);
            test.pass(confirmationMessage);
        } catch (ComparisonFailure e) {
            test.fail(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            test.fail(e.getMessage());
            e.printStackTrace();
        }

    }
}
