import constants.Constants;
import constants.TestData;
import org.junit.ComparisonFailure;
import org.junit.Test;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class AddItemToWishListTest extends TestBase implements TestData {

    @Test
    public void addToWishList() {
        try {
            test = extentReports.createTest("Test Add Wishlist",
                    "Add an item into wishlist");
            homePage.clickOnSearch();
            homePage.typeSearchKeyword(SEARCH_TXT);
            homePage.hitEnterKey();

            itemPage.clickAddProductToWishList();
            itemPage.openPage(Constants.URL + "/wishlist/");

            wishlistPage.refreshPage();
            List<String> productList = wishlistPage.getAddedWishlistProducts();

            boolean isContain = productList.contains(SEARCH_TXT);
            assertTrue(isContain);
            test.pass(String.valueOf(isContain));
        } catch (ComparisonFailure e) {
            test.fail(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            test.fail(e.getMessage());
            e.printStackTrace();
        }
    }
}
