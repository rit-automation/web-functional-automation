package factory;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pages.*;


public class PageFactory {

    public BaseClass getPageObject(WebDriver driver, String pageName) {

        BaseClass baseClass = null;

        if (pageName.equalsIgnoreCase("Home Page")) {
            baseClass = new HomePage(driver);
        } else if (pageName.equalsIgnoreCase("Cart Page")) {
            baseClass = new CartPage(driver);
        } else if (pageName.equalsIgnoreCase("Checkout Page")) {
            baseClass = new CheckoutPage(driver);
        } else if (pageName.equalsIgnoreCase("Item Page")) {
            baseClass = new ItemPage(driver);
        } else if (pageName.equalsIgnoreCase("Wishlist Page")) {
            baseClass = new WishlistPage(driver);
        } else {
            Assert.assertNotNull(pageName + " is not expected page", baseClass);
        }

        return baseClass;
    }
}
