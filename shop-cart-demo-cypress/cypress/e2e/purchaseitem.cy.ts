import { ADDRESS, CITY, COLOR, CONFIRMATION_MESSAGE, EMAIL, FIRST_NAME, ITME_TITLE, LAST_NAME, PHONE_NO, POSTAL_CODE, SIZE, URL } from "../data/testdata";
import cartpage from "../pages/cartpage";
import checkoutpage from "../pages/checkoutpage";
import homepage from "../pages/homepage";
import itempage from "../pages/itempage";

describe('Purchase item', () => {
    it('Purchase an item from shopping cart', () => {
      cy.visit(URL);
      homepage.getSearchLabel.click();
      homepage.enterSearchText(ITME_TITLE);
      homepage.hitEnter();  
      
      itempage.selectColor(COLOR);
      itempage.selectSize(SIZE);
      itempage.clickAddToCart();
      itempage.clickViewCart();

      cartpage.clickCheckoutProceed();

      checkoutpage.enterFirstName(FIRST_NAME);
      checkoutpage.enterLastName(LAST_NAME);
      checkoutpage.enterAddress(ADDRESS);
      checkoutpage.enterCity(CITY);
      checkoutpage.enterPostalCode(POSTAL_CODE);
      checkoutpage.enterPhone(PHONE_NO);
      checkoutpage.enterEmail(EMAIL);
      checkoutpage.clickOnTerms();
      checkoutpage.clickOnPlaceOrder();
      checkoutpage.verifyConfirmationMessage(CONFIRMATION_MESSAGE);
    })
  })