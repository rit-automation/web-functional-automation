import { ITME_TITLE, URL } from "../data/testdata";
import homepage from "../pages/homepage"
import itempage from "../pages/itempage";
import wishlistpage from "../pages/wishlistpage";

describe('Add to wishlist', () => {
  it('Add an item into wishlist', () => {
    cy.visit(URL)
    homepage.getSearchLabel.click();
    homepage.enterSearchText(ITME_TITLE);
    homepage.hitEnter();  
    

    itempage.clickAddWishListButton();

    cy.visit(URL + '/wishlist');
    cy.reload();
    cy.location('pathname', {timeout: 60000}).should('include', '/wishlist');

    wishlistpage.verifyAddedItemInWishList("PINK DROP SHOULDER OVERSIZED T SHIRT");
  })
})
