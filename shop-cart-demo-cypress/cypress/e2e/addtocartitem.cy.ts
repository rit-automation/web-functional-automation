import { COLOR, ITME_TITLE, SIZE, URL } from "../data/testdata";
import cartpage from "../pages/cartpage";
import homepage from "../pages/homepage";
import itempage from "../pages/itempage";

describe('Add to cart', () => {
    it('Add an item into Shopping cart', () => {
      cy.visit(URL);
      homepage.getSearchLabel.click();
      homepage.enterSearchText(ITME_TITLE);
      homepage.hitEnter();  
      
      itempage.selectColor(COLOR);
      itempage.selectSize(SIZE);
      itempage.clickAddToCart();
      itempage.clickViewCart() ;
      
      cartpage.verifyProductTitle(ITME_TITLE, COLOR);
      cartpage.verifyProductSize(SIZE);
    })
  })