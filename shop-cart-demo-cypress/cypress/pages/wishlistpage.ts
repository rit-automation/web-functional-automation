class WishlistPage {

    verifyAddedItemInWishList(itemTitle : string) {
        cy.get('.product-name > a').invoke('text')
        .should("contain", itemTitle.toLocaleLowerCase());
    }
}

export default new WishlistPage();
