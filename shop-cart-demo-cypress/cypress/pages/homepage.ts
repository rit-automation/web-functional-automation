class HomePage {

    get getSearchLabel() {
        return cy.get('.noo-search');
    }

    enterSearchText(searchText : string) {
        return cy.get('.form-control').type(searchText);
    }

    hitEnter () {
        cy.get('.form-control').type('{enter}');
    }
}

export default new HomePage();
