class ItemPage {

    clickAddWishListButton() {
        cy.wait(2000);
        cy.get('.summary > .yith-wcwl-add-to-wishlist > .yith-wcwl-add-button > .add_to_wishlist').click();
    }

    selectColor(color:string) {
        cy.get('#pa_color').select(color);
    }

    selectSize(size : string) {
        cy.get('#pa_size').select(size);
    }

    clickAddToCart() {
        cy.get('.single_add_to_cart_button').click();
    }

    clickViewCart() {
        cy.get('.woocommerce-message > .button').click();
    }
}

export default new ItemPage;
