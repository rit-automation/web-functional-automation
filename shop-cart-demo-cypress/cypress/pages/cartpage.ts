import { String } from "cypress/types/lodash";

class CartPage{

    clickCheckoutProceed() {
        cy.get('.wc-proceed-to-checkout > .checkout-button').click();
    }

    verifyProductTitle(itemTitle : string, color : string) {
        cy.reload();
        cy.get('.product-name > a').invoke('text').should("contain"
        .toLocaleLowerCase(), itemTitle.toLocaleLowerCase() + " - " +color);
    }

    verifyProductSize(itemSize : string) {
        cy.get('dd.variation-Size > p').invoke('text').should("contain", itemSize);
    }
}

export default new CartPage();
