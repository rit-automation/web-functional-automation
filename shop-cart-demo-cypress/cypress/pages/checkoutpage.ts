class CheckoutPage {

    enterFirstName(firstName : string) {
        cy.get('#billing_first_name').type(firstName);
    }

    enterLastName(lastName : string) {
        cy.get('#billing_last_name').type(lastName);
    }

    enterAddress(address : string) {
        cy.get('#billing_address_1').type(address);
    }

    enterCity(city : string) {
        cy.get('#billing_city').type(city);
    }

    enterPostalCode(postalCode : string) {
        cy.get('#billing_postcode').type(postalCode);
    }

    enterPhone(phoneNo : string) {
        cy.get('#billing_phone').type(phoneNo);
    }

    enterEmail(email :string) {
        cy.get('#billing_email').type(email);
    }

    clickOnTerms() {
        cy.wait(2000);
        cy.get('div.blockUI.blockOverlay').should('not.exist');
        cy.get('#terms').click();
    }

   clickOnPlaceOrder() {
    cy.get('#place_order').click();
   }  

   verifyConfirmationMessage(message : string) {
    cy.get('.page-title').scrollIntoView();
    cy.get('.woocommerce-thankyou-order-received').invoke('text').should("contain", message);
   }
}

export default new CheckoutPage();
