# Shopping cart demo

### Table of Content
- [Introduction](#Introduction)
- [Prerequisites](#Prerequisites)
- [Test configuration](#Test-configuration)
- [Running tests locally](#Running-tests-locally)
- [Technology stack](#Technology-stack)
- [Report Generation](#Report-generation)

## Introduction

This project contains UI functional tests Ui mode and headless mode for Shop cart demo 

### Prerequisites
- [cypress 10.9.0](https://docs.cypress.io/guides/references/changelog)
- [cypress-mochawesome-reporter 3.2.2](https://www.npmjs.com/package/cypress-mochawesome-reporter)
- [mochawesome-merge 4.2.1](https://www.npmjs.com/package/mochawesome-merge)
- [mochawesome-report-generator 6.2.0](https://www.npmjs.com/package/mochawesome-report-generator)
- [typescript: 4.8.4](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-4-8.html)  

### Test Configuration
Setup following script inside package json
"clean-reports": "rmdir /S /Q cypress\\report && mkdir cypress\\report && mkdir cypress\\report\\mochawesome-report"
"combine-reports": "mochawesome-merge ./cypress/report/mochawesome-report/*.json > ./cypress/report/report.json"
"generate-report": "marge cypress/report/report.json --reportDir ./ --inline"

# Run single test cases with ui visual
1. npx cypress open
2. Select 'E2E' Testing
3. Select a browser type ex:- Chrome
4. click on "Start E2E Testing in Chrome"
5. Select a single test which you need to run

# Run All test cases headless mode and generate test report
1. npm run clean-reports 
2. npx cypress run
3. npm run combine-reports
4. npm run generate-report
5. Right click on "report.html" in "report" folder copy path and paste on browser.

# Run Single test cases headless mode and generate test report
1. npm run clean-reports 
2. npx cypress run --spec cypress\e2e\<test-case-name>.cy.ts
3. npm run combine-reports
4. npm run generate-report
5. Right click on "report.html" in "report" folder copy path and paste on browser.

### Technology stack
- [cypress 10.9.0](https://docs.cypress.io/guides/references/changelog)
- [cypress-mochawesome-reporter 3.2.2](https://www.npmjs.com/package/cypress-mochawesome-reporter)
- [mochawesome-merge 4.2.1](https://www.npmjs.com/package/mochawesome-merge)
- [mochawesome-report-generator 6.2.0](https://www.npmjs.com/package/mochawesome-report-generator)
- [typescript: 4.8.4](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-4-8.html)
    