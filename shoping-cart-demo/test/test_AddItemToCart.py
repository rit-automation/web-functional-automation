import unittest

import pytest

import constant
from pages.cartpage import CartPage
from pages.homepage import HomePage
from pages.itempage import ItemPage
from test.basetest import driver


class AddItemToCartTest(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def classSetup(self, driver):
        self.homepage = HomePage(driver)
        self.item = ItemPage(driver)
        self.cart = CartPage(driver)
        self.driver = driver

    def test1_search_product_item(self):
        print("Search an Item")
        self.homepage.click_lbl_search()
        self.homepage.enter_search_text(constant.PRODUCT_NAME)
        self.homepage.enter_key_pressed()

    def test2_add_to_shopping_cart(self):
        print("Add an item into shopping cart")
        title = self.item.get_item_title(driver, 50)
        assert title == constant.PRODUCT_NAME

        self.item.select_colour(constant.COLOUR)
        self.item.select_size(constant.SIZE)
        self.item.click_on_add_to_cart()
        self.item.click_on_cart(driver, 50)

        assert self.cart.get_item_title(driver, 50) == constant.PRODUCT_NAME + " - " \
               + constant.COLOUR.upper()
