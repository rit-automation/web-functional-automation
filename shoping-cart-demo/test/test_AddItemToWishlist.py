import unittest

import pytest

import constant
from pages.homepage import HomePage
from pages.itempage import ItemPage
from pages.wishlistpage import WishlistPage
from test.basetest import driver


@pytest.mark.usefixtures("driver")
class AddItemToWishlistTest(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def classSetup(self, driver):
        self.homepage = HomePage(driver)
        self.item = ItemPage(driver)
        self.wishlist = WishlistPage(driver)
        self.driver = driver

    def test1_search_product_item(self):
        print("Search an Item")
        self.homepage.click_lbl_search()
        self. homepage.enter_search_text(constant.PRODUCT_NAME)
        self. homepage.enter_key_pressed()

    def test2_add_to_wishlist(self):
        print("Add an item into wishlist")
        self.item.click_add_product_to_wish_list()

        self.driver.get(constant.URL + "/wishlist")
        self.driver.refresh()
        products = self.wishlist.get_added_item_item_list()

        assert products == constant.PRODUCT_NAME
