import unittest

import pytest

import constant
from pages.cartpage import CartPage
from pages.checkoutpage import CheckoutPage
from pages.homepage import HomePage
from pages.itempage import ItemPage
from pages.wishlistpage import WishlistPage
from test.basetest import driver


class PurchaseItemTest(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def classSetup(self, driver):
        self.homepage = HomePage(driver)
        self.wishlist = WishlistPage(driver)
        self.itemPage = ItemPage(driver)
        self.cartPage = CartPage(driver)
        self.checkoutPage = CheckoutPage(driver)

    def test1_add_an_item_to_shopping_cart(self):
        print("Add an item into shopping cart")
        self.homepage.click_lbl_search()
        self.homepage.enter_search_text(constant.PRODUCT_NAME)
        self.homepage.enter_key_pressed()
        title = self.itemPage.get_item_title(driver, 50)
        assert constant.PRODUCT_NAME == title

        self.itemPage.select_colour(constant.COLOUR)
        self.itemPage.select_size(constant.SIZE)
        self.itemPage.click_on_add_to_cart()
        self.itemPage.click_on_cart(driver, 30)

        title = self.cartPage.get_item_title(driver, 30)
        assert constant.PRODUCT_NAME + " - " + constant.COLOUR.upper() == title

        size = self.cartPage.get_item_size()
        assert constant.SIZE == size

        self.cartPage.click_on_proceed_to_check_out()

    def test2_checkout_item(self):
        print("Checkout an item")
        self.checkoutPage.enter_first_name(driver, constant.FIRST_NAME, 30)
        self.checkoutPage.enter_last_name(constant.LAST_NAME)
        self.checkoutPage.enter_address(constant.ADDRESS)
        self.checkoutPage.enter_city(constant.CITY)
        self.checkoutPage.enter_postal_code(constant.POSTAL_CODE)
        self.checkoutPage.enter_phone_no(constant.PHONE_NO)
        self.checkoutPage.enter_email(constant.EMAIL)
        self.checkoutPage.scroll_to_element(self.driver, constant.SCROLL_COORDINATES)
        self.checkoutPage.click_on_terms(driver, constant.TEN_SECONDS_WAIT_TIME)
        self.checkoutPage.click_on_place_order()

        confirmation_message = self.checkoutPage.get_confirmation_message()

        assert confirmation_message == constant.CONFIRMATION_MESSAGE
