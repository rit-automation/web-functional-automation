import unittest

import pytest

import drivers
from constant import URL


@pytest.fixture(scope="class")
def driver(request):
    print("Started running, setting up driver one time..")
    driver = drivers.getchromedriver()
    request.cls.driver = driver
    driver.get(URL)
    driver.maximize_window()
    yield driver
    driver.quit()
