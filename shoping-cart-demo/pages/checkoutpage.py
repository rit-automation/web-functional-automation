from pages.baseclass import BaseClass


class CheckoutPage(BaseClass):

    locators = {
        "btnCheckout": ("XPATH", "//a[contains(text(),'Proceed to checkout')]"),
        "tfFname": ("ID", "billing_first_name"),
        "tfLname": ("ID", "billing_last_name"),
        "tfAddress": ("ID", "billing_address_1"),
        "tfCity": ("ID", "billing_city"),
        "tfPostalCode": ("ID", "billing_postcode"),
        "tfPhone": ("ID", "billing_phone"),
        "tfEmail": ("ID", "billing_email"),
        "btnPlaceOrder": ("ID", "place_order"),
        "confirmationMessage": ("CSS", "div>p.woocommerce-thankyou-order-received"),
        "chkTermsNote": ("CSS", "span.woocommerce-terms-and-conditions-checkbox-text"),
        "blockUi": ("CSS", "div.blockUI.blockOverlay")
    }

    def enter_first_name(self, webdriver, text, time):
        self.wait_element_visible(webdriver, self.tfFname, time)
        self.enter_text(self.tfFname, text)

    def enter_last_name(self, text):
        self.enter_text(self.tfLname, text)

    def enter_address(self, text):
        self.enter_text(self.tfAddress, text)

    def enter_city(self, text):
        self.enter_text(self.tfCity, text)

    def enter_postal_code(self, text):
        self.enter_text(self.tfPostalCode, text)

    def enter_phone_no(self, text):
        self.enter_text(self.tfPhone, text)

    def enter_email(self, text):
        self.enter_text(self.tfEmail, text)

    def click_on_terms(self, webdriver, time):
        self.wait_element_invisible(webdriver, time, self.blockUi)
        self.click(self.chkTermsNote)

    def click_on_place_order(self):
        self.click(self.btnPlaceOrder)

    def get_confirmation_message(self):
        return self.get_text_value(self.confirmationMessage)

