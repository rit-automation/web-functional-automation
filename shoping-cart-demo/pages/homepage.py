from selenium.webdriver.common.keys import Keys

from pages.baseclass import BaseClass


class HomePage(BaseClass):

    locators = {
        "lblSearch": ("XPATH", "//a[@class='noo-search']/i"),
        "txtSearch": ("XPATH", "//input[@type='search']"),
    }

    def enter_search_text(self, text):
        self.enter_text(self.txtSearch, text)

    def click_lbl_search(self):
        self.click(self.lblSearch)

    def enter_key_pressed(self):
        self.txtSearch.send_keys(Keys.ENTER)
