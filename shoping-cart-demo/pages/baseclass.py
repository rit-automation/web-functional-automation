from selenium.webdriver.support.wait import WebDriverWait
from seleniumpagefactory import PageFactory
from selenium.webdriver.support import expected_conditions as ec


class BaseClass(PageFactory):
    def __init__(self, driver):
        self.driver = driver

    def enter_text(self, element, text):
        element.send_keys(text)

    def click(self, element):
        element.click()

    def get_text_value(self, element):
        return element.get_text()

    def scroll_to_element(self, webdriver, coordinates):
        webdriver.execute_script("window.scrollTo(" + coordinates + ")")

    def wait_element(self, webdriver):
        webdriver.manage().timeouts().pageLoadTimeout(10, "Seconds")

    def wait_element_invisible(self, webdriver, time, element):
        wait = WebDriverWait(webdriver, time)
        wait.until(ec.invisibility_of_element(element))

    def wait_element_visible(self, webdriver, element, time):
        wait = WebDriverWait(webdriver, time)
        wait.until(ec.visibility_of(element))
