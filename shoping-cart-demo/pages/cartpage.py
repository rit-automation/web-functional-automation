from pages.baseclass import BaseClass


class CartPage(BaseClass):

    locators = {
        "lblItemTitle": ("XPATH", "(//tr[@class='cart_item']/td/a)[2]"),
        "lblSize": ("XPATH", "//dl//dd//p"),
        "btnViewCart": ("XPATH", "//a[contains(text(),'View cart')]"),
        "btnCheckout": ("XPATH", "//a[contains(text(),'Proceed to checkout')]")
    }

    def get_item_title(self, webdriver, time):
        self.wait_element_visible(webdriver, self.lblItemTitle, time)
        return self.lblItemTitle.get_text()

    def get_item_size(self):
        return self.lblSize.get_text()

    def click_on_view_cart(self):
        self.click(self.btnViewCart)

    def click_on_proceed_to_check_out(self):
        self.click(self.btnCheckout)
