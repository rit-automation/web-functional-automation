from pages.baseclass import BaseClass


class WishlistPage(BaseClass):
    locators = {
        "wishlistAddedItem": ("XPATH", "//tbody//tr//td[@class='product-name']//a"),
    }

    def get_added_item_item_list(self):
        return self.get_text_value(self.wishlistAddedItem)
