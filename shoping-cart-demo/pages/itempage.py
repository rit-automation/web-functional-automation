from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select

from pages.baseclass import BaseClass


class ItemPage(BaseClass):

    locators = {
        "lblItemTitle": ("CSS", "h1.product_title.entry-title"),
        "ddColour": ("ID", "pa_color"),
        "ddSize": ("ID", "pa_size"),
        "btnAddToCart": ("XPATH", "//button[contains(text(),'Add to cart')]"),
        "btnCart": ("XPATH", "//a[@title='View cart']"),
        "btnAddToWishList": ("CSS", "div.summary.entry-summary>div>div.yith-wcwl-add-button")
    }

    def get_item_title(self, webdriver, time):
        return self.get_text_value(self.lblItemTitle)

    def click_lbl_search(self):
        self.click(self.lblSearch)

    def enter_key_pressed(self):
        self.txtSearch.send_keys(Keys.ENTER)

    def select_colour(self, text):
        select = Select(self.ddColour)
        select.select_by_visible_text(text)

    def select_size(self, text):
        select = Select(self.ddSize)
        select.select_by_visible_text(text)

    def click_on_add_to_cart(self):
        self.click(self.btnAddToCart)

    def click_on_cart(self, webdriver, time):
        self.wait_element_visible(webdriver, self.btnCart, time)
        self.click(self.btnCart)

    def click_add_product_to_wish_list(self):
        self.click(self.btnAddToWishList)
