# Shopping cart demo 

### Table of Content
- [Introduction](#Introduction)
- [Prerequisites](#Prerequisites)
- [Test configuration](#Test-configuration)
- [Running tests locally](#Running-tests-locally)
- [Technology stack](#Technology-stack)
- [Report Generation](#Report-generation)

## Introduction

This project contains UI functional and visual automated tests for Shop cart demo 

### Prerequisites

- [Python 3.10.7](https://www.python.org/downloads/release/python-369/)
- [PyTest 7.1.3](https://docs.pytest.org/en/7.1.x/announce/release-7.0.1.html)
- [pip 22.2.2](https://pip.pypa.io/en/stable/user_guide/)
- [pytest-html 3.1.1](https://docs.pytest.org/en/7.1.x/announce/release-3.1.1.html)
- [Selenium 4.4.3](https://www.selenium.dev/selenium/docs/api/py/api.html)
- [Chrome browser has to be installed](https://www.google.com/chrome/?brand=CHBD&gclid=Cj0KCQjwgJv4BRCrARIsAB17JI7-EIZCQfo4Je5O0LNbMME2sYLjDzlcpM6zeRW81u3ehLyeK8Ebz7MaAnnIEALw_wcB&gclsrc=aw.ds)
- [Setup default IDE formatting tool](https://gitlab.com/rit-automation/web-functional-automation/wiki/Setup-default-IDE-formatting-tool)
- [Selenium page factory](https://selenium-page-factory.readthedocs.io/en/latest/)


# Run all tests
 pytest -v --html=report.html --self-contained-html <test_package_name>\

# example
 pytest -v --html=report.html --self-contained-html test\

# Run single test
pytest -v --html=report.html --self-contained-html <package_name>\<test_class_name>

# example
pytest -v --html=report.html --self-contained-html test\test1_AddItemToCart.py


### Technology stack

- [Python 3.6.9](https://www.python.org/downloads/release/python-369/)
- [pip 22.2.2](https://pip.pypa.io/en/stable/user_guide/)
- [PyTest 7.0,1](https://docs.pytest.org/en/7.1.x/announce/release-7.0.1.html)
- [Selenium 4.4.3](https://www.selenium.dev/selenium/docs/api/py/api.html)
- [pytest-html 3.1.1](https://docs.pytest.org/en/7.1.x/announce/release-3.1.1.html)
- [Selenium page factory](https://selenium-page-factory.readthedocs.io/en/latest/)

### Report generation
 # Right click on "report.html" file and "Open IN -> Browser -> Chrome"
